(ns cs378hw2.core)
;function for asking user input
(fn userprompt [] (do (print"What would you like to eat")
                     (read-line)))
;read users input for restaurant options
(defn insert [userprompt] ( if userprompt (true) 
                            (readline) ))
;parser to search input to find restaurant for user
(defn parser [insert]( instance-> Restaurants[instance]
                       cond-> [](loop[match (Restaurant)]
                                            result[Restaurant])))
;function that define the characterists 
;that make up a estaurant listing
(defn Restaurants :name :type :rating
  :pricerange :location :chainvsindepend
  :glutenfree :organic/freerange
  :localproducts)
;a function for name of the restaurants
(fn name [Restaurants] str (Restaurant) )
;a function for the type of food
;served at the restaurant
(fn type [Restaurants](or 
  :pizza :vegetarian :vegan 
  :seafood :ethnic :american
  :french))
;a function for the rating of the
;restaurant
(fn rating [Restaurants](or 
  :unrated :* :** :***))
;price range of food served at the 
;restaurant
(fn pricerange [Restaurants](or
  :$5-$15 :$10-$20 
  :$15-$25 :$25-$50))
;location of the restaurant from a user's
;location
(fn location [Restaurant](or
  :.5-.9 :1.0-1.9
  :2.0-2.9  :3.0-3.9
  :4.0-4.9 :5.0-5.9))
;a function for whether or not the 
;restaurant is independently owned 
;or a chain
(fn chainvsindepend [Restaurant](or
  :chain 
  :independent))
;if the restaurant serves gluten free food
(fn glutenfree [Restaurant](or 
  :glutenfree nil))
;for whether or not the restaurant uses
;organic and/or free range ingredients
(fn organic/freerange [Restaurant](or 
  :organic 
  :freerange nil))
;for whether or not the restaurant uses
;local ingredients 
(fn localproducts [Restaurant](or
  :localproducts nil))
;list of restaurants
(Restaurant ( :name "It's Only Natural"
              :type vegetarian
              :rating **
              :pricerange $15-$25
              :location 1.0
              :chainvsindepend independent
              :glutenfree glutenfree
              :organic/freerange organic
              :localproducts localproducts))
(Restaurant ( :name "Kate's Cafe"
              :type vegan
              :rating **
              :pricerange $15-$25
              :location 2.3
              :chainvsindepend independent
              :glutenfree glutenfree
              :organic/freerange organic
              :localproducts localproducts))
(Restaurant ( :name "Franz's Food"
              :type vegetarian
              :rating **
              :pricerange $5-$15
              :location .5
              :chainvsindepend independent
              :glutenfree glutenfree
              :organic/freerange organic
              :localproducts localproducts))
(Restaurant ( :name "Candle 79"
              :type vegan
              :rating ***
              :pricerange $25-$50
              :location 5.0
              :chainvsindepend independent
              :glutenfree glutenfree
              :organic/freerange organic
              :localproducts nil))
(Restaurant ( :name "Domino's"
              :type pizza
              :rating *
              :pricerange $5-$15
              :location 1.2
              :chainvsindepend chain
              :glutenfree nil
              :organic/freerange nil
              :localproducts nil))
(Restaurant ( :name "Pepe's Pizza"
              :type pizza
              :rating ***
              :pricerange $10-$20
              :location 2.0
              :chainvsindepend chain
              :glutenfree glutenfree
              :organic/freerange organic
              :localproducts localproducts))
(Restaurant ( :name "Jack's Restaurant"
              :type pizza
              :rating **
              :pricerange $10-$20
              :location .5
              :chainvsindepend independent
              :glutenfree glutenfree
              :organic/freerange organic
              :localproducts localproducts))
(Restaurant ( :name "Christopher's"
              :type pizza
              :rating ***
              :pricerange $5-$15
              :location 3.0
              :chainvsindepend chain
              :glutenfree nil
              :organic/freerange nil
              :localproducts nil))
(Restaurant ( :name "Northern India"
              :type ethnic
              :rating **
              :pricerange $10-$20
              :location 2.2
              :chainvsindepend independent
              :glutenfree nil
              :organic/freerange nil
              :localproducts nil))
(Restaurant ( :name "Lucky House"
              :type ethnic
              :rating *
              :pricerange $5-$15
              :location 1.0
              :chainvsindepend chain
              :glutenfree nil
              :organic/freerange nil
              :localproducts nil))
(Restaurant ( :name "Margarita's"
              :type ethnic
              :rating **
              :pricerange $15-$25
              :location 4.0
              :chainvsindepend chain
              :glutenfree nil
              :organic/freerange nil
              :localproducts nil))
(Restaurant ( :name "Black Sheep"
              :type american
              :rating **
              :pricerange $15-$25
              :location .5
              :chainvsindepend independent
              :glutenfree glutenfree
              :organic/freerange organic freerange
              :localproducts localproducts))
(Restaurant ( :name "Brown's"
              :type seafood
              :rating ***
              :pricerange $5-$15
              :location 3.0
              :chainvsindepend independent
              :glutenfree nil
              :organic/freerange nil
              :localproducts localproducts))
(Restaurant ( :name "The Spot"
              :type american
              :rating unrated
              :pricerange $10-$20
              :location 1.7
              :chainvsindepend chain
              :glutenfree nil
              :organic/freerange freerange
              :localproducts localproducts))
(Restaurant ( :name "Silver Moon"
              :type french
              :rating ***
              :pricerange $5-$15
              :location .3
              :chainvsindepend independent
              :glutenfree glutenfree
              :organic/freerange organic
              :localproducts localproducts))
;prints the result of the user input
(def output [Restaurant] (do (println result[Restaurant]))) 
  
  
  
            
